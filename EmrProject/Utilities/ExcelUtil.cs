﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace EmrProject.Utilities
{
    public class ExcelUtil
    {
            public static object[] GetsheetInttoObject(string fileDetails, string sheetName)
            {
                Excel.Application app = new Excel.Application();
                Excel.Workbook book = app.Workbooks.Open(fileDetails);                
                Excel.Worksheet sheet = book.Worksheets[sheetName];               
                Excel.Range range = sheet.UsedRange;
                int rowCount = range.Rows.Count;
                int colCount = range.Columns.Count;           
                object[] main = new object[rowCount - 1];
            for (int i = 2; i <= rowCount; i++)
                {
                    object[] temp = new object[colCount];

                    for (int j = 1; j <= colCount; j++)
                    {
                        string value = range.Cells[i, j].value;
                        Console.WriteLine(value);
                        temp[j - 1] = value;
                    }
                    main[i - 2] = temp;
                }

                book.Close();
                app.Quit();
                return main;
            }        
    }
}
