﻿using EmrProject.pagefactory.OpenEmr;
using EmrProject.pagefactory.RoyalCaribbean;
using EmrProject.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmrProject.Tests
{
    public class RoyalCaribbeanTestCases : BaseTest
    {

        [Test]
        public void ExploreShips()
        {
            driver.Url = "https://www.royalcaribbean.com/";

            HomePageCaribbean homepagedetails = new HomePageCaribbean(driver);
            bool whalewatch = homepagedetails.CheckWhaleWatchElementPresent();
            homepagedetails.HamburgerClick();
            homepagedetails.ExperienceClick();
            driver.FindElement(By.Id("rciHeaderSideNavSubmenu-2-2")).Click();       
            homepagedetails.RhapsodyClick();          
            homepagedetails.DeckPlansClick();            
            Assert.IsTrue(whalewatch & homepagedetails.CheckRoyalSuiteElementPresent(), "Does not meet my requirements");
           
        }
        public static object[] ExploreCruisesData()
        {
            object[] main = ExcelUtil.GetsheetInttoObject(@"D:\svsb365\selenium\EmrProject\EmrProject\Data\OpenEMRData.xlsx","ExploreCruises");
            return main;

        }


        [Test,TestCaseSource("ExploreCruisesData")]
        public void ExploreCruises(string actualday1, string actualday2,string actualday3)
        {
            driver.Url = "https://www.royalcaribbean.com/";
            
            HomePageCaribbean homepagedetails = new HomePageCaribbean(driver);
            homepagedetails.HamburgerClick();
            homepagedetails.PlanCruise();
            homepagedetails.FindCruise();            
            homepagedetails.SamplerCruise();
            homepagedetails.ViewItineraryDetails();
            homepagedetails.ExploreItinerary();
            Thread.Sleep(1000);
            homepagedetails.ViewItinerary();                        
            StringAssert.AreEqualIgnoringCase(actualday1, homepagedetails.Day1Text());
            StringAssert.AreEqualIgnoringCase(actualday2, homepagedetails.Day2Text());            
            StringAssert.AreEqualIgnoringCase(actualday3, homepagedetails.Day3Text());

        }
        public static object[] SignUpData()
        {
            object[] main = ExcelUtil.GetsheetInttoObject(@"D:\svsb365\selenium\EmrProject\EmrProject\Data\OpenEMRData.xlsx", "SignUp");
            return main;

        }
        [Test,TestCaseSource("SignUpData")]
        public void SignUp(string firstname, string lastname, string dobyear, string email,string password,string ans)
        {
            driver.Url = "https://www.royalcaribbean.com/";
            SignUpPage Signupinfo = new SignUpPage(driver);
            Signupinfo.SignUpClick();
            Signupinfo.CreateAnAccount();
            Signupinfo.FirstName(firstname);
            Signupinfo.LastName(lastname);
            Signupinfo.SelectMonth();
            Signupinfo.SelectMonthSelection();            
            Signupinfo.SelectDate();
            Signupinfo.SelectDateSelection();
            Signupinfo.YearSelection(dobyear);
            Signupinfo.CountrySelection();
            Signupinfo.CountrySelect();
            Signupinfo.Email(email);
            Signupinfo.Password(password);
            Signupinfo.SeceurityQuestion();
            Signupinfo.SecurityQuestionSelection();
            Signupinfo.SecurityAns(ans);
            Signupinfo.CheckBoxClick();
            Signupinfo.DoneClick();

        }
    }
    }

