﻿using EmrProject.pagefactory.OpenEmr;
using EmrProject.Utilities;
using NUnit.Framework;
using NUnit.Framework.Internal.Execution;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EmrProject.Tests
{
    public class OpenEmrTestCases : BaseTest
    {
        public static string PatientName = string.Empty;

        public static object[] AddPatientData()
        {
            object[] main = ExcelUtil.GetsheetInttoObject(@"D:\svsb365\selenium\EmrProject\EmrProject\Data\OpenEMRData.xlsx", "AddPatient");
            return main;

        }

        [Test, TestCaseSource("AddPatientData")]
        public void AddPatient(string username, string password, string language, string expectedValue,string firstname,string lastname,string dob,string gender)
        {
            driver.Url = "https://demo.openemr.io/b/openemr/interface/login/login.php?site=default";

            LoginPage objLoginPage = new LoginPage(driver);
            HomePage objHomePage = new HomePage(driver);
            PatienDetailPage objPaitentDetailPage = new PatienDetailPage(driver);
            objLoginPage.SendUsername(username);
            objLoginPage.SendPassword(password);
            objLoginPage.selectlanguage(language);
            objLoginPage.ClickOnLogin();
            string actualvalue = objHomePage.GetCurrentTitle();
            Assert.AreEqual(expectedValue, actualvalue);          
            objPaitentDetailPage.PaitentClick();
            objPaitentDetailPage.Newsearchpaitent();
            Thread.Sleep(4000);
            objPaitentDetailPage.SwitchToPatFrame();
            objPaitentDetailPage.Paitentfirstname(firstname);
            PatientName = firstname;
            objPaitentDetailPage.Paitentlastname(lastname);
            objPaitentDetailPage.paitentDOB(dob);
            objPaitentDetailPage.paitentsex(gender);     
            objPaitentDetailPage.paitentcreate();
            Thread.Sleep(4000);
            driver.SwitchTo().DefaultContent();
            objPaitentDetailPage.SwitchToModalFrame();
            objPaitentDetailPage.CreatePaitentClick();
            driver.SwitchTo().DefaultContent();
            objPaitentDetailPage.alert();
        }

         public static object[] RecallBoardpaitentData()
        {
            object[] main = ExcelUtil.GetsheetInttoObject(@"D:\svsb365\selenium\EmrProject\EmrProject\Data\OpenEMRData.xlsx","RecallBoardpaitent");
            return main;

        }

        [Test,TestCaseSource("RecallBoardpaitentData")]
        public void RecallBoardpaitent(string username, string password, string language,string name,string recalldate,string providername,string facilitiesname,string providersearch,string facilitiessearch)
        {
            driver.Url = "https://demo.openemr.io/b/openemr/interface/login/login.php?site=default";


            LoginPage login = new LoginPage(driver);
            PatienDetailPage paitentdetails = new PatienDetailPage(driver);
            login.SendUsername(username);
            login.SendPassword(password);
            login.selectlanguage(language);
            login.ClickOnLogin();
            Thread.Sleep(1000);
            paitentdetails.RecallClick();
            Thread.Sleep(1000);
             paitentdetails.SwitchTorcbFrame();
            Thread.Sleep(1000);
            paitentdetails.ReminderRecall();
            driver.SwitchTo().DefaultContent();            
            paitentdetails.SwitchTorecallFrame();
            Thread.Sleep(1000);
            paitentdetails.PaitentRecall();
            driver.SwitchTo().DefaultContent();
            paitentdetails.SwitchToModalFrame();
            paitentdetails.PaitentSearch(name);
            paitentdetails.SubmitClick();
            paitentdetails.NameSelect();
            driver.SwitchTo().DefaultContent();
            paitentdetails.SwitchTorecallFrame();
            Thread.Sleep(1000);
            paitentdetails.RecallDateClear();
            paitentdetails.RecallDate(recalldate);
            paitentdetails.ProviderSelect(providername);
            paitentdetails.FacilitieSelect(facilitiesname);
            paitentdetails.AddRecall();
            paitentdetails.PaitentNameSearch(name);
            paitentdetails.ProviderSearch(providersearch);
            paitentdetails.FacilitieSearch(facilitiessearch);
       

            string paitentname = driver.FindElement(By.XPath($"//div[@id='rcb_table']//a[contains(text(),'{name}')]")).Text;
            Console.WriteLine(paitentname);

            string paitentid = driver.FindElement(By.XPath($"//div[@id='rcb_table']//a[contains(text(),'{name}')]/../..//span[@title='Patient ID']")).Text;
            Console.WriteLine(paitentid);

            string paitentDob = driver.FindElement(By.XPath($"//div[@id='rcb_table']//a[contains(text(),'{name}')]/../..//span[@title='Date of Birth and Age']")).Text;
            Console.WriteLine(paitentDob);

            IWebElement delete = driver.FindElement(By.XPath($"//div[@id='rcb_table']//a[contains(text(),'{name}')]/../..//i"));
            delete.Click();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(x => x.SwitchTo().Alert()).Accept();

        }
    }
}
