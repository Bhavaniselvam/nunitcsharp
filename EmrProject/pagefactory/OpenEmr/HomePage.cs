﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrProject.pagefactory.OpenEmr
{
    class HomePage
    {
        private By patientclientloc = By.XPath("//div[text()='Patient/Client']");
       

        private readonly IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        
        public string GetCurrentTitle()
        {
            return driver.Title;
        }


    }
}

    

