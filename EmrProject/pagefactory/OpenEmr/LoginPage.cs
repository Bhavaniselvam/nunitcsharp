﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrProject.pagefactory.OpenEmr
{
    public class LoginPage
    {

        private By userLocator = By.Id("authUser");
        private By PasswordLocator = By.Id("clearPass");
        private By langlocator = By.Name("languageChoice");
        private By lodinlocator = By.XPath("//button[@type='submit']");



        private IWebDriver driver;
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void SendUsername(string username)
        {
            driver.FindElement(userLocator).SendKeys(username);
        }

        public void SendPassword(string password)

        {
            driver.FindElement(PasswordLocator).SendKeys(password);
        }

        public void selectlanguage(string language)
        {
            IWebElement langEle = driver.FindElement(langlocator);
            SelectElement selectLanguage = new SelectElement(langEle);
            selectLanguage.SelectByText(language);
        }

        public void ClickOnLogin()
        {
            driver.FindElement(lodinlocator).Click();
        }  
    }
}




  