﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrProject.pagefactory.OpenEmr
{
    public class PatienDetailPage
    {

        private By paitentLoc = By.XPath("//div[text()='Patient/Client']");
        private By newsearchpaitentloc = By.XPath("//div[text()='New/Search']");
        private By paitentfirstnameloc = By.Id("form_fname");
        private By paitentlastnameloc = By.Id("form_lname");
        private By paitentdobloc = By.Id("form_DOB");
        private By paitentsexloc = By.Id("form_sex");
        private By paitentcreateloc = By.Id("create");
        private By PaitentModalFrameLoc = By.XPath("//iframe[@id='modalframe']");
        private By PaitentRecallFrameLoc = By.XPath("//iframe[@name='recall']");
        private By PaitentPatFrameLoc = By.XPath("//iframe[@name='pat']");
        private By PaitentRecallLoc = By.Id("new_recall_name");
        private By PaitentSearchlLoc = By.Id("searchparm");
        private By PaitentSubmitLoc = By.Id("submitbtn");
        private By PaitentNameSelectLoc = By.XPath("//table[@class='table table-condensed']/tbody/tr[1]/td[1]");
        private By PaitentRecallClickLoc = By.XPath("(//div[text()='Recall Board'])[1]");
        private By PaitentRemainderRecallLoc = By.XPath("//button[contains(@onclick,'goReminderRecall')]");
        private By PaitentSwitchTorcbFrameLoc = By.XPath("//iframe[@name='rcb']");
        private By PaitentRecallDateClearLoc = By.Id("form_recall_date");
        private By PaitentRecallDateLoc = By.Id("form_recall_date");
        private By PaitentSelectlLoc = By.Id("new_provider");
        private By PaitentFacilitiesSelectLoc = By.Id("new_facility");
        private By PaitentAddRecallLoc = By.Id("add_new");
        private By PaitentNameSearchlLoc = By.Id("form_patient_name");
        private By PaitentProviderSearchlLoc = By.Id("form_provider");
        private By PaitentFacilitiesSearchlLoc = By.Id("form_facility");
        private By PaitentCreateClickLoc = By.XPath("//input[@value='Confirm Create New Patient']");


        private IWebDriver driver;

        public PatienDetailPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void PaitentClick()
        {
            driver.FindElement(paitentLoc).Click();
        }
        public void Newsearchpaitent()
        {
            driver.FindElement(newsearchpaitentloc).Click();
        }

        public void Paitentfirstname(string paitentfirstname)
        {
            driver.FindElement(paitentfirstnameloc).SendKeys(paitentfirstname);
        }

        public void Paitentlastname(string paitentlastname)
        {

            driver.FindElement(paitentlastnameloc).SendKeys(paitentlastname);

        }
        public void paitentDOB(string dob)
        {
            driver.FindElement(paitentdobloc).SendKeys(dob);

        }
        public void paitentsex(string value)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
            wait.Until(x => x.FindElement(paitentsexloc));

            IWebElement Sexele = driver.FindElement(paitentsexloc);
            SelectElement selectsex = new SelectElement(Sexele);
            selectsex.SelectByText(value);
        }
        public void paitentcreate()
        {
            driver.FindElement(paitentcreateloc).Click();
        }

        public void CreatePaitentClick()
        {
          
            driver.FindElement(PaitentCreateClickLoc).Click();

        }
        public void SwitchToModalFrame()
        {
            IWebElement modalframe = driver.FindElement(PaitentModalFrameLoc);
            driver.SwitchTo().Frame(modalframe);
        }
        public void alert()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(x => x.SwitchTo().Alert()).Accept();
        }
       

        public void SwitchToPatFrame()
        {

            IWebElement pat = driver.FindElement(PaitentPatFrameLoc);
            driver.SwitchTo().Frame(pat);
        }
        public void SwitchTorecallFrame()
        {

            IWebElement recall = driver.FindElement(PaitentRecallFrameLoc);
            driver.SwitchTo().Frame(recall);
        }
        public void PaitentRecall()
        {

            driver.FindElement(PaitentRecallLoc).Click();
        }

        public void PaitentSearch(string value)
        {

            driver.FindElement(PaitentSearchlLoc).SendKeys(value);


        }
        public void SubmitClick()
        {

            driver.FindElement(PaitentSubmitLoc).Click();
        }

        public void NameSelect()
        {

            driver.FindElement(PaitentNameSelectLoc).Click();

        }

        public void RecallClick()
        {


            driver.FindElement(PaitentRecallClickLoc).Click();

        }
        public void ReminderRecall()
        {

            driver.FindElement(PaitentRemainderRecallLoc).Click();
        }

        public void SwitchTorcbFrame()
        {

            IWebElement rcb = driver.FindElement(PaitentSwitchTorcbFrameLoc);
            driver.SwitchTo().Frame(rcb);
        }

        public void RecallDateClear()
        {

            driver.FindElement(PaitentRecallDateClearLoc).Clear();
        }

        public void RecallDate(string value)
        {

            driver.FindElement(PaitentRecallDateLoc).SendKeys(value);
        }
        public void ProviderSelect(string value)
        {

            IWebElement providerele = driver.FindElement(PaitentSelectlLoc);
            SelectElement selectprovider = new SelectElement(providerele);
            selectprovider.SelectByText(value);

        }

        public void FacilitieSelect(string value)
        {

            IWebElement facelitiele = driver.FindElement(PaitentFacilitiesSelectLoc);
            SelectElement selectfacelities = new SelectElement(facelitiele);
            selectfacelities.SelectByText(value);
        }

        public void AddRecall()
        {

            driver.FindElement(PaitentAddRecallLoc).Click();
        }
        public void PaitentNameSearch(string value)
        {


            driver.FindElement(PaitentNameSearchlLoc).SendKeys(value);

        }
        public void ProviderSearch(string value)
        {

            IWebElement providerformele = driver.FindElement(PaitentProviderSearchlLoc);
            SelectElement selectproviderform = new SelectElement(providerformele);
            selectproviderform.SelectByText(value);
        }
        public void FacilitieSearch(string value)
        {

            IWebElement facelitiformele = driver.FindElement(PaitentFacilitiesSearchlLoc);
            SelectElement selectfacelitiesform = new SelectElement(facelitiformele);
            selectfacelitiesform.SelectByText(value);
        }
        public void RecallFilter()
        {

            driver.FindElement(PaitentRecallLoc).Click();

        }


    }

    }
