﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrProject.pagefactory.RoyalCaribbean
{
    class SignUpPage
    {
        private By SignUpClickLoc = By.Id("rciHeaderSignIn");
        private By CreateAnAccountLoc = By.XPath("//a[contains(text(),'Create an account')]");
        private By FirstNameLoc = By.Id("mat-input-3");
        private By LastNameLoc = By.Id("mat-input-4");
        private By SelectMonthLoc = By.XPath("(//span[contains(text(),'Month')])[1]");
        private By SelectMonthSelectionLoc = By.XPath("//span[contains(text(),'May')]");
        private By SelectDateLoc = By.XPath("(//span[contains(text(),'Day')])[1]");
        private By SelectDateSelectionLoc = By.Id("mat-option-23");
        private By YearSelectionLoc = By.Id("mat-input-5");
        private By CountrySelectionLoc = By.XPath("//span[contains(text(),'Country/Region of residence')]");
        private By CountrySelectLoc = By.XPath("//span[contains(text(),' India ')]");
        private By EmailLoc = By.Id("mat-input-2");
        private By PasswordLoc = By.Id("mat-input-6");
        private By SeceurityQuestionLoc = By.Id("mat-select-3");
        private By SecurityQuestionSelectionLoc = By.Id("mat-option-295");
        private By SecurityAnsLoc = By.Id("mat-input-7");
        private By DoneClickLoc = By.XPath("//button[contains(text(),' Done ')]");
        private By CheckBoxClickLoc = By.Id("mat-checkbox-2-input");

        private IWebDriver driver;

        public SignUpPage(IWebDriver driver)
        {
            this.driver = driver;
        }


        public void SignUpClick()
        { 
            

            driver.FindElement(SignUpClickLoc).Click();
        }
        public void CreateAnAccount()
        {
            

            driver.FindElement(CreateAnAccountLoc).Click();
        }
        public void FirstName(string value)
        {

            driver.FindElement(FirstNameLoc).SendKeys(value);
        }
        public void LastName(string value)
        {


            driver.FindElement(LastNameLoc).SendKeys(value);
        }
        public void SelectMonth()
        {


            driver.FindElement(SelectMonthLoc).Click();
        }
        public void SelectMonthSelection()
        {
            driver.FindElement(SelectMonthSelectionLoc).Click();
        }
        public void SelectDate()
        {

            driver.FindElement(SelectDateLoc).Click();
        }
        public void SelectDateSelection()
        {

            driver.FindElement(SelectDateSelectionLoc).Click();
        }
        public void YearSelection(string value)
        {

            driver.FindElement(YearSelectionLoc).SendKeys(value);

        }

        public void CountrySelection()
        {

            driver.FindElement(CountrySelectionLoc).Click();

        }
        public void CountrySelect()
        {
            //private By CountrySelectLoc = By.XPath("//span[contains(text(),' India ')]");

            driver.FindElement(CountrySelectLoc).Click();
        }
        public void Email(string value)
        {

            driver.FindElement(EmailLoc).SendKeys(value);
        }
        public void Password(string value)
        {
            driver.FindElement(PasswordLoc).SendKeys(value);
        }
        public void SeceurityQuestion()
        {

            driver.FindElement(SeceurityQuestionLoc).Click();
        }
        public void SecurityQuestionSelection()
        {

            //private By SecurityQuestionSelectionLoc = By.Id("mat-option-295");
            driver.FindElement(SecurityQuestionSelectionLoc).Click();
        }
        public void SecurityAns(string value)
        {

            driver.FindElement(SecurityAnsLoc).SendKeys(value);
        }


        public void CheckBoxClick()
        {

            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", driver.FindElement(CheckBoxClickLoc));


        }
        public void DoneClick()
        {


            driver.FindElement(DoneClickLoc).Click();
        }

    }
}
