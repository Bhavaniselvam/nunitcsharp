﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrProject.pagefactory.RoyalCaribbean
{
    class HomePageCaribbean
    {
        private By HamburgerLoc = By.Id("rciHeaderOpenSidenav");
        private By ExperienceClickLoc = By.Id("rciHeaderSideNavMenu-2");
        private By DeckPlansClickLoc = By.XPath("//a[text()='DECK PLANS']");
        private By WhaleWatchingLink = By.XPath("//*[contains(text(),'Whale Watching')]");
        private By OurShipLoc = By.Id("rciHeaderSideNavSubmenu-2-2");
        private By RhapsodyClickLoc = By.XPath("//p[contains(text(),'Rhapsody of the Seas')]");
        private By RoyalSuiteLink = By.XPath("//*[contains(text(),'Royal Suite')]");
        private By PlanCruiseLoc = By.Id("rciHeaderSideNavMenu-1");
        private By FindCruiseLoc = By.Id("rciHeaderSideNavSubmenu-1-1");
        private By SamplerCruiseLoc = By.XPath("(//h3[contains(text(),' Sampler Cruise')])[1]");
        private By ViewItineraryDetailsLoc = By.Id("explore_itinerary");
        private By ExploreItineraryLoc = By.XPath("//span[contains(text(),'Explore This Itinerary')]");
        private By ViewItineraryLoc = By.XPath("(//span[contains(text(),'View Itinerary')])[2]");
        private By Day1TextLoc = By.XPath("(//div[@class='explore-panel__nav-item'])[3]");
        private By Day2TextLoc = By.XPath("(//div[@class='explore-panel__nav-item'])[4]");
        private By Day3TextLoc = By.XPath("(//div[@class='explore-panel__nav-item'])[5]");



        private IWebDriver driver;

        public HomePageCaribbean(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void HamburgerClick()
        {
            driver.FindElement(HamburgerLoc).Click();
        }
        public void ExperienceClick()
        {
            driver.FindElement(ExperienceClickLoc).Click();

        }

        public void RhapsodyClick()
        {
            driver.FindElement(RhapsodyClickLoc).Click();
        }

        public void OurShip()
        {

            driver.FindElement(OurShipLoc).Click();
        }
        public Boolean CheckWhaleWatchElementPresent()
        {
            bool value = false;
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                value = wait.Until(x => x.FindElement(WhaleWatchingLink).Displayed);
            }
            catch (Exception e)
            {
                value = false;
            }
            return value;
        }

        public Boolean CheckRoyalSuiteElementPresent()
        {
            bool value = false;
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                value = wait.Until(x => x.FindElement(RoyalSuiteLink).Displayed);
            }
            catch (Exception e)
            {
                value = false;
            }
            return value;
        }

        public void DeckPlansClick()
        {
            driver.FindElement(DeckPlansClickLoc);
        }
        public void PlanCruise()
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", driver.FindElement(PlanCruiseLoc));
            //private By PlanCruiseLoc = By.Id("rciHeaderSideNavMenu-1");
           // driver.FindElement(PlanCruiseLoc).Click();
        }
        public void FindCruise()
        {

            driver.FindElement(FindCruiseLoc).Click();
        }
        public void SamplerCruise()
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", driver.FindElement(SamplerCruiseLoc));

        }
        public void ViewItineraryDetails()
        {

            driver.FindElement(ViewItineraryDetailsLoc).Click();
        }
        public void ExploreItinerary()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
            wait.Until(x => x.FindElement(ExploreItineraryLoc));

            driver.FindElement(ExploreItineraryLoc).Click();
        }

        public void ViewItinerary()
        {
            driver.FindElement(ViewItineraryLoc).Click();
        }

        public string Day1Text()
        {

            string ExpectedDay1 = driver.FindElement(Day1TextLoc).Text;
            Console.WriteLine(ExpectedDay1);
            return ExpectedDay1;
        }
        public string Day2Text()
        {


            string ExpectedDay2 = driver.FindElement(Day2TextLoc).Text;
            Console.WriteLine(ExpectedDay2);
            return ExpectedDay2;
        }
        public string Day3Text()
        {

            string ExpectedDay3 = driver.FindElement(Day3TextLoc).Text;
            Console.WriteLine(ExpectedDay3);
            return ExpectedDay3;
        }

    }
}